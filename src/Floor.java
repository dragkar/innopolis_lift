import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by admin on 16.12.2017.
 */
public class Floor {
    //номер этажа
    private int floorNumber;
    //лист людей, ждущих лифт
   private List<Person> listPersons = new ArrayList<>();
    //монитор вызовов вверх и вниз значение 0 - отсутсвует 1 - есть вызов

    ConcurrentLinkedQueue<Person> queueUp = new ConcurrentLinkedQueue<>();
    ConcurrentLinkedQueue<Person> queueDown = new ConcurrentLinkedQueue<>();
  //  public int[] state = new int[]{0, 0};

    public Floor(int floorNumber) {
        this.floorNumber = floorNumber;
        setPersons();
    }

    //заполняем список с пассажирами
    public void setPersons() {

        Random random = new Random();

        int n = random.nextInt(4) + 1;
        for (int i = 0; i < n; i++) {
            Person person = new Person(floorNumber);
            if (person.getDestination() > floorNumber) {
              //  state[0] = 1;
                queueUp.add(person);
            } else {
             //   state[1] = 1;
                queueDown.add(person);
            }
           // listPersons.add(person);
        }
    }

    public int getFloorNumber() {
        return floorNumber;
    }
}

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentMap;

/**
 * Created by admin on 15.12.2017.
 */

public class Main {
    //колличество этажей
    static final int N = 10;
    //очередь вызововю
    static ConcurrentLinkedQueue<Integer> queue = new ConcurrentLinkedQueue<>();
static ConcurrentMap<Integer, Floor> mapFloor  = new ConcurrentHashMap<>();

    public static void main(String[] args) {

        makeFloors(N);

      Lift lift = new Lift();
        Thread threadLifat = new Thread(lift);
        threadLifat.start();
    }

    private static void makeFloors(int n) {
        for (int i = 1; i <= n; i++) {
            Floor floor = new Floor(i);

            queue.add(i);
            mapFloor.put(i, floor);
        }
    }
}

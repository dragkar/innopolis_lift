import java.util.Random;

/**
 * Created by admin on 15.12.2017.
 */
public class Person {
    //местонахождение
    private int location;

    //место назначения
    private int destination = 0;

    public int getLocation() {
        return location;
    }

    public Person(int location) {
        this.location = location;
        setDestination();
    }

    public int getDestination() {
        return destination;
    }

    public void setDestination() {

        Random random = new Random();
        while (this.destination == location || this.destination == 0 )
        this.destination = random.nextInt(Main.N);
    }


}

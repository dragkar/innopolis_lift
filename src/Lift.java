import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by admin on 15.12.2017.
 */
public class Lift implements Runnable {
    //скорость лифта в секундах от этажа к этажу
    private final int speed = 1000;
    //текущее положение лифта
    private int currentPosition = 1;
    //место назначения из лифта
    private int destinationFromLift = 1;
    // признак занятости лифта
    private boolean isBusy = false;
    //текущее колличество пассажиров и максимальное допустимое число
    private final int MAX_PASSANGER = 10;
    int countPassanger = 0;
    //пассажиры
    List<Person> listPassenger = new ArrayList<>();
    //очередь вызовов из лифта
    ConcurrentLinkedQueue<Integer> queueFromLift = new ConcurrentLinkedQueue<>();
    //направление движения
    private State state = State.Neitral;

    public int getCurrentPosition() {
        return currentPosition;
    }

    public void setCurrentPosition(int currentPosition) {
        this.currentPosition = currentPosition;
    }

    public int getDestinationFromLif() {
        return destinationFromLift;
    }

    public void setDestinationFromLif(int destination) {
        this.destinationFromLift = destination;
    }

    @Override
    public void run() {
        while (true) {
            elevatorCall();
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void elevatorCall() {

        if (isBusy) {
            if (queueFromLift.size() > 0) {
                sendLift();

            } else {
                isBusy = false;
            }
        } else {
            if (Main.queue.size() > 0) {
                setDestinationFromLif(Main.queue.peek());
                state = getDestinationFromLif() >= getCurrentPosition() ? State.GoingUp : State.GoingDown;
                try {
                    if (Main.queue.size() > 0) {
                        Thread.sleep(Math.abs(getCurrentPosition() - getDestinationFromLif()) * speed);
                        isBusy = true;

                        System.out.println("-------------------------");
                        System.out.println("Лифт на " + getDestinationFromLif() + " этаже");
                        addPassangers(getDestinationFromLif());
                        sendLift();
                    } else isBusy = false;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    //добавление пассажиров в массив лифта + добавление в очередь этажи назначения,
// если списки пассажиров на этаже пусты, то удаляем его из очереди
    private void addPassangers(int currentPosition) {

        setCurrentPosition(currentPosition);
        while (countPassanger < MAX_PASSANGER) {
            //проверяем, в какую сторону идет лифт. Затем заполняем его(по умолчанию, при значении State.Neitral лифт идет вверх)

            if (Main.mapFloor.get(currentPosition).queueUp.size() > 0 && (state == State.GoingUp || state == State.Neitral)) {

                queueFromLift.add(Main.mapFloor.get(currentPosition).queueUp.peek().getDestination());
                listPassenger.add(Main.mapFloor.get(currentPosition).queueUp.poll());
                countPassanger++;
                System.out.println("-------------------------");
                System.out.println("В лифт зашел еще один пассажир. Сейчас в лифте " + countPassanger + " пассажиров");
                //  state = State.GoingUp;

            } else if (Main.mapFloor.get(currentPosition).queueDown.size() > 0) {

                queueFromLift.add(Main.mapFloor.get(currentPosition).queueDown.peek().getDestination());
                listPassenger.add(Main.mapFloor.get(currentPosition).queueDown.poll());
                countPassanger++;
                //   state = State.GoingDown;
                System.out.println("++++++++++++++++");
                System.out.println("В лифт зашел еще один пассажир. Сейчас в лифте " + countPassanger + " пассажиров");
            } else {
                //зануляем кнопки вызова
                Main.queue.remove(currentPosition);
                queueFromLift.remove(currentPosition);
                break;
            }
        }
        //зануляем кнопки вызова
//        if (Main.mapFloor.get(Main.queue.peek()).queueDown.size() == 0)Main.mapFloor.get(Main.queue.peek()).state[1] = 0;
//        if (Main.mapFloor.get(Main.queue.peek()).queueUp.size() == 0)Main.mapFloor.get(Main.queue.peek()).state[0] = 0;
        // Main.queue.add(Main.queue.poll());
    }

    // отправка лифта на следующую точку
    private void sendLift() {
        if (queueFromLift.size() > 0) {
            setDestinationFromLif(queueFromLift.poll());
            if (getDestinationFromLif() < getCurrentPosition())
                state = State.GoingDown;
            else state = State.GoingUp;
            System.out.println("-------------------------");
            System.out.println("Лифт направляется на " + getDestinationFromLif());
            while (getCurrentPosition() != getDestinationFromLif()) {
                try {
                    Thread.sleep(speed);

                    if (state == State.GoingDown) {
                        setCurrentPosition(getCurrentPosition() - 1);
                    } else if (state == State.GoingUp || state == State.Neitral) {
                        setCurrentPosition(getCurrentPosition() + 1);
                    }
                    System.out.println("-------------------------");
                    System.out.println("Лифт находится на " + (getCurrentPosition()) + " этаже");
                    removePassanger();
                    addPassangers(getCurrentPosition());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void removePassanger() {
        List<Person> tempList = listPassenger;
        for (int i = 0; i < listPassenger.size(); i++) {
            if (listPassenger.get(i).getDestination() == getCurrentPosition()) {
                listPassenger.remove(listPassenger.get(i));
                countPassanger--;
                System.out.println("-------------------------");
                System.out.println("Пассажир покинул лифт");
                System.out.println("Сейчас в лифте " + countPassanger + " пассажиров");
            }
        }
    }
}
